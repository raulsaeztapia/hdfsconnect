name := "HDFSConnector"
organization := "com.thingso2"

scalaVersion := "2.11.12"

publishTo := version { v: String =>
  val nexus = "https://repo.thingso2.com/"
  if (v.trim.endsWith("SNAPSHOT"))
    Some("snapshots" at nexus + "repository/maven-snapshots")
  else
    Some("releases" at nexus + "repository/maven-releases")
}.value

resolvers ++= Seq(
  "ThingsO2 Snapshots" at "https://repo.thingso2.com/repository/maven-snapshots",
  "ThingsO2 Releases" at "https://repo.thingso2.com/repository/maven-releases",
  "confluent" at "http://packages.confluent.io/maven/"
)

libraryDependencies ++= Seq(
  "org.apache.kafka" % "connect-runtime" % "1.0.0",
  "org.apache.kafka" % "connect-json" % "1.0.0",
  "com.typesafe.scala-logging" %% "scala-logging-slf4j" % "2.1.2"
)
