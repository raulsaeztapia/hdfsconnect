package com.thingso2.hdfsconnector.sink

import java.util
import javax.jms.Message
import javax.jms.MessageConsumer
import javax.jms.TextMessage
import scala.collection.JavaConverters._
import scala.util.Failure
import scala.util.Success

import com.typesafe.scalalogging.slf4j.LazyLogging
import org.apache.kafka.connect.source.SourceRecord
import org.apache.kafka.connect.source.SourceTask


class HDFSSinkTask extends SourceTask with LazyLogging {

/*  private var jmsSessionProvider: HDFSSessionProvider = _
  private var consumer: MessageConsumer = _
  private var topic: String = _
  private var connectorVersion: String = _

  override def start(props: util.Map[String, String]): Unit = {
    logger.debug("Starting JMS Source Task")
    topic = props.get(HDFSSinkConnectorConfig.KAFKA_TOPIC)
    connectorVersion = props.get(HDFSSinkConnectorConfig.CONNECTOR_VERSION)
    jmsSessionProvider = HDFSSessionProvider(props)
    val queue = jmsSessionProvider.createQueue(props.get(HDFSSinkConnectorConfig.JMS_QUEUE_NAME))
    queue.flatMap(q => jmsSessionProvider.getConsumer(q)) match {
      case Success(c) => this.consumer = c
      case Failure(error) => throw error
    }
  }

  override def poll(): util.List[SourceRecord] = {
    logger.debug("polling...")
    val message: Message = consumer.receive()
    List(convert(topic, message)).asJava
  }

  private def convert(topic: String,  message: Message): SourceRecord = {
    message match {
      case textMessage: TextMessage =>
        new SourceRecord(null, null, topic, null, textMessage.getText)
      case _ =>
        logger.debug("No text message: Only TextMessages handled")
        throw new Exception("No text message: Only TextMessages handled")
    }
  }

  override def stop(): Unit = {
    logger.debug("Stopping JMS Source Task")
    jmsSessionProvider.close()
  }

  override def version(): String = connectorVersion*/

  override def start(props: util.Map[String, String]): Unit = ???

  override def poll(): util.List[SourceRecord] = ???

  override def stop(): Unit = ???

  override def version(): String = ???
}
