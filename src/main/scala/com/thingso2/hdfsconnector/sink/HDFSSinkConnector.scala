package com.thingso2.hdfsconnector.sink

import java.util

import com.typesafe.scalalogging.slf4j.LazyLogging
import org.apache.kafka.common.config.ConfigDef
import org.apache.kafka.connect.connector.Task
import org.apache.kafka.connect.source.SourceConnector

class HDFSSinkConnector extends SourceConnector with LazyLogging {

/*  private var configProps: util.Map[String, String] = _
  private var topic: String = _
  private var jmsUrl: String = _
  private var jmsQueueName: String = _
  private val connectorVersion: String = getClass.getPackage.getImplementationVersion

  override def taskClass(): Class[_ <: Task] = classOf[HDFSSinkTask]

  override def start(props: util.Map[String, String]): Unit = {
    logger.info("Starting JMS Source Connector")
    configProps = props
    topic = props.get(HDFSSinkConnectorConfig.KAFKA_TOPIC)
    jmsUrl = props.get(HDFSSinkConnectorConfig.JMS_URL)
    jmsQueueName = props.get(HDFSSinkConnectorConfig.JMS_QUEUE_NAME)
  }

  override def stop(): Unit = { }

  override def taskConfigs(maxTasks: Int): util.List[util.Map[String, String]] = {
    logger.debug("propagating connector properties")
    val configs: util.ArrayList[util.Map[String, String]] = new util.ArrayList[util.Map[String, String]]()
    val config: util.Map[String, String] = new util.HashMap[String, String]()
    config.put(HDFSSinkConnectorConfig.KAFKA_TOPIC, topic)
    config.put(HDFSSinkConnectorConfig.JMS_URL, jmsUrl)
    config.put(HDFSSinkConnectorConfig.JMS_QUEUE_NAME, jmsQueueName)
    config.put(HDFSSinkConnectorConfig.CONNECTOR_VERSION, connectorVersion)
    configs.add(config)
    configs
  }

  override def version(): String = connectorVersion

  override def config(): ConfigDef = new ConfigDef()*/

  override def version(): String = ???

  override def start(props: util.Map[String, String]): Unit = ???

  override def taskClass(): Class[_ <: Task] = ???

  override def taskConfigs(maxTasks: Int): util.List[util.Map[String, String]] = ???

  override def stop(): Unit = ???

  override def config(): ConfigDef = ???
}
