package com.thingso2.hdfsconnector.sink

object HDFSSinkConnectorConfig {

  private val CONNECTOR_PREFIX = "connect.jms"
  private val KAFKA_PREFIX = "connect.kafka"

  // KAFKA CONFIG PARAMS KEYS
  val KAFKA_TOPIC = s"$KAFKA_PREFIX.kafka.topic"

  // ACTIVE MQ CONFIG PARAMS KEYS
  val JMS_URL = s"$CONNECTOR_PREFIX.host"
  val JMS_QUEUE_NAME = s"$CONNECTOR_PREFIX.queue.name"

  // CONNECTOR CONFIG PARAMS KEYS
  val CONNECTOR_VERSION = s"$CONNECTOR_PREFIX.version"
}
