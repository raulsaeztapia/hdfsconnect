package com.thingso2.hdfsconnector

import java.util


trait Configuration {

  val env: util.Map[String, String] = System.getenv()

  // JMS Config params
  val HDFS_URL: String = env.getOrDefault("JMS_URL", "tcp://127.0.0.1:8090")

  // Kafka Config params
  val KAFKA_TOPIC: String = env.getOrDefault("KAFKA_TOPIC", "jms")
  val KAFKA_BOOTSTRAP_SERVERS: String = env.getOrDefault("KAFKA_BOOTSTRAP_SERVERS", "127.0.0.1:9092")
}
